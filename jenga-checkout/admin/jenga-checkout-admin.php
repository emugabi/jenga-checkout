<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://gitlab.com/emugabi/jenga-checkout
 * @since      1.0.2
 *
 * @package    Jenga_Checkout
 * @subpackage Jenga_Checkout/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Jenga_Checkout
 * @subpackage Jenga_Checkout/admin
 * @author     Mugabi Elvis <elvis@a23labs.com>
 */
class Jenga_Checkout_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.2
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $__plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.2
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
	private $__version;
	
	/**
	 * The security nonce
	 *
	 * @var string
	 */
    private $_nonce = 'jenga_checkout_admin';
    

    private $jengaUtils;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.2
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->__plugin_name = $plugin_name;
        $this->__version     = $version;

        $this->jengaUtils = new JenjaUtils();
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Jenga_Checkout_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Jenga_Checkout_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->__plugin_name, plugin_dir_url(__FILE__) . 'css/plugin-name-admin.css', array(), $this->__version, 'all');

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         *
         * An instance of this class should be passed to the run() function
         * defined in Jenga_Checkout_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Jenga_Checkout_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        //wp_enqueue_script($this->__plugin_name, JENGA_URL. 'assets/js/jenga.js', array(), 1.0);
        
        wp_enqueue_script($this->__plugin_name, plugin_dir_url(__FILE__) . 'js/jenga-checkout-admin.js', array('jquery'), $this->__version, false);
        
		$admin_options = array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'_nonce'   => wp_create_nonce( $this->_nonce ),
        );
        
		wp_localize_script($this->__plugin_name, 'jenga_exchanger', $admin_options);

    }

    /**
     * Adds the Jenga label to the WordPress Admin Sidebar Menu
     */
    public function addAdminMenu()
    {

        $page_title = __($this->__plugin_name, 'jenga-checkout');
        $menu_title = __($this->__plugin_name, 'jenga-checkout');
        $capability = 'manage_options';
        $menu_slug  = 'jenga-checkout';
        $function   = array($this, 'adminLayout');
        $icon_url   = 'dashicons-testimonial';

        add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function, $icon_url);
	}
	
	/**
	 * Callback for the Ajax request
	 *
	 * Updates the options data
     *
     * @return void
	 */
	public function storeAdminData()
    {
		if (wp_verify_nonce($_POST['security'], $this->_nonce ) === false)
			die('Invalid Request! Reload your page please.');
        
        $data = $this->getData();

        //$_data = [];
        
        foreach ($_POST as $field => $value) {
            // $_data[$field] = $value;
		    if (substr($field, 0, 6) !== "jenga_")
                continue;
            
            // We remove the jenga_ prefix to clean things up
            $field = substr($field, 6);
            
		    if (empty($value))
		        unset($data[$field]);
		    
			$data[$field] = esc_attr__($value);
        }
        
        $this->updateOptions($data);
        
		echo __('Saved API Settings!', 'jenga-checkout');
		die();
    }

    /**
     * Outputs the Admin Dashboard layout containing the form with all its options
     *
     * @return void
     */
    public function adminLayout()
    {
        $data = $this->getData();
        
        $merchantCode = ($data['merchant_code'] ?? '');
        $apiKey = ($data['api_key'] ?? '');
        $password = ($data['password'] ?? '');

        $token = $this->jengaUtils->getPaymentToken($this->__plugin_name);

        if(empty($token))
            $token = $this->jengaUtils->requestPaymentToken($merchantCode, $apiKey, $password);
 
        ?>

        <div class="wrap">
            <h3><?php _e('Jenga API Settings', 'jenga-checkout'); ?></h3>
    
            <p>
            <?php _e('You can get your Jenga API credentials by following the <a href="https://developer.jengapgw.io/docs/web-checkout-form" target="_blank">official</a> documentations.', 'jenga-checkout'); ?>
            </p>

            <hr>
    
            <form id="jenga-admin-form">
    
                <table class="form-table">
                    <tbody>
                    <tr>
                            <td scope="row">
                                <label><?php _e( 'API Endpoint', 'jenga-checkout' ); ?></label>
                            </td>
                            <td>
                                <input name="jenga_api_endpoint"
                                id="jenga_api_endpoint"
                                class="regular-text"
                                placeholder="http:// or https://"
                                value="<?php echo $data['api_endpoint'] ?? JENGA_ENDPOINT; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">
                                <label><?php _e( 'API key', 'jenga-checkout' ); ?></label>
                            </td>
                            <td>
                                <input name="jenga_api_key"
                                id="jenga_api_key"
                                class="regular-text"
                                value="<?php echo (isset($data['api_key'])) ? $data['api_key'] : ''; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">
                                <label><?php _e( 'Merchant Code', 'jenga-checkout' ); ?></label>
                            </td>
                            <td>
                                <input name="jenga_merchant_code"
                                id="jenga_merchant_code"
                                class="regular-text"
                                value="<?php echo (isset($data['merchant_code'])) ? $data['merchant_code'] : ''; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">
                                <label><?php _e( 'API Password', 'jenga-checkout' ); ?></label>
                            </td>
                            <td>
                                <input name="jenga_password"
                                id="jenga_password"
                                class="regular-text"
                                type="text"
                                value="<?php echo (isset($data['password'])) ? $data['password'] : ''; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button class="button button-primary" id="jenga-admin-save" type="submit"><?php _e( 'Save', 'jenga-checkout' ); ?></button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <hr>
                                <h4><?php _e( 'Tokens', 'jenga-checkout' ); ?></h4>
                            </td>
                        </tr>
                        <?php if (!empty($data['api_key']) && !empty($data['merchant_code']) && !empty($data['password'])): ?>
                        <?php
                            // if we don't even have a response from the API
                            if (empty($token)) : ?>
                                <tr>
                                    <td>
                                        <p class="notice notice-error">
                                        <?php _e( 'No active token found. Try refreshing this page to automatically generate a token.', 'jenga-checkout' ); ?>
                                        </p>
                                    </td>
                                </tr>
                        <?php
                            // If we have an error returned by the API
                            elseif (isset($token['error'])): ?>
                        <tr>
                            <td>
                                <p class="notice notice-error">
                                <?php echo json_encode($token); ?>
                                </p>
                            </td>
                        </tr>
                        <?php
                            // If the tokens were returned
                            else: ?>
                        <tr>
                            <td>
                                <p class="notice notice-success">
                                <?php _e( 'Payment token successfully generated!', 'jenga-checkout' ); ?>
                                </p>
                                <table>
                                    <tr>
                                    <td>
                                    Active Token
                                    </td>
                                    <td>
                                    <?php _e(json_encode($token['payment-token']), 'jenga-token' ); ?>
                                    </td>
                                    </tr>
                                </table>
                                <div>
                                <!-- <button class="button button-primary" id="jenga-generate-token" type="submit"><?php _e( 'REGENERATE', 'jenga-checkout' ); ?></button> -->
                                </div>
                                <hr>
                        </tr>
                        <?php endif; ?>
                        <?php else: ?>
                        <tr>
                            <td>
                                <p>Please fill up your API keys to see the recent transactions.</p>
                            </td>
                        </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
    
            </form>
 
	    </div>

    <?php

    }


    public function getData(){
        return $this->jengaUtils->getPluginOptions();
    }

    public function updateOptions($data){
        return $this->jengaUtils->updatePluginOptions($data);
    }

}
