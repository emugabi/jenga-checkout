<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://gitlab.com/emugabi/jenga-checkout
 * @since      1.0.0
 *
 * @package    Jenga_Checkout
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
