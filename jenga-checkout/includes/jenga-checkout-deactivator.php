<?php

/**
 * Fired during plugin activation
 *
 * @link       http://gitlab.com/emugabi/jenga-checkout
 * @since      1.0.0
 *
 * @package    jenga_checkout
 * @subpackage jenga_checkout/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    jenga_checkout
 * @author     Mugabi Elvis <elvis@a23labs.com>
 */
class Jenga_Checkout_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
