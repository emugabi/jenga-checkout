<?php

class JenjaUtils {

    	/**
	 * The option name
	 *
	 * @var string
	 */
    private $__option_name = 'jenga_checkout_data';
    
    /**
	 * The cache key
	 *
	 * @var string
	 */
    private $__key_token_cache = 'jenga_payment_token';

    /**
	 * Updates plugin options with $data
     *
     * @return array
	 */
    public function updatePluginOptions($data){

       return update_option($this->__option_name, $data);
    }

    /**
	 * Returns the saved options data as an array
     *
     * @return array
	 */
	public function getPluginOptions()
    {
	    return get_option($this->__option_name, array());
    }

    /** 
     * Makes an API call to the Jenja API and returns the response. 
     * @param $data array
     * @return array
     */
    public function getPaymentToken($generateIfMissing = false){

        // get plugin data
        $data = $this->getPluginOptions();

        // check if token exists in cache
        $_token = wp_cache_get( $this->__key_token_cache );

        if ( false === $_token && $generateIfMissing) {

            if(!isset($data['api_key']) || !isset($data['merchant_code']) || !isset($data['password']))
                return ['error' => 'Incomplete API credentials'];

            $merchantCode = ($data['merchant_code']);
            $apiKey = ($data['api_key']);
            $password = ($data['password']);

            $_token = $this->requestPaymentToken($merchantCode, $apiKey, $password);

            $data = $this->getPluginOptions();
            $data['token'] = $_token;
            update_option($this->__option_name, $data);
      
            wp_cache_set( $this->__key_token_cache, $_token );
        }
      
        return $_token;

    }


	/**
	 * Make an API call to the Jenga API and returns the response
     *
     * @param $merchantCode string
     * @param $apiKey string
     * @param $password string
     * @param $grantType string|null
     *
     *
     * @return array
	 */
	public static function requestPaymentToken($merchantCode, $apiKey, $password, $grantType = "password")
    {
        $data = array();

        $body = array('merchantCode' => $merchantCode, 'password' => $password, 'grant_type' => $grantType);
        $endpoint = (isset($data['api_endpoint'])) ? $data['api_endpoint'] : JENGA_ENDPOINT;
        $url = $endpoint.'/v1/token';
        $headers = array('Content-type' => 'application/json', 'Authorization' => 'Basic '.$apiKey);

        $payload = array(
            'method' => 'POST',
            'body' => json_encode($body), 
            'headers' =>  $headers
        );
        
        $response = wp_remote_post($url, $payload);
        
	    if (is_array($response) && !is_wp_error($response)) {
		    $data = json_decode($response['body'], true);
	    }
	    return $data;
    }

}
