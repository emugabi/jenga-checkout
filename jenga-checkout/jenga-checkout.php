<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://gitlab.com/emugabi/jenga-checkout.git
 * @since             1.0.0
 * @package           Jenga_Checkout
 *
 * @wordpress-plugin
 * Plugin Name:       Jenga Checkout
 * Plugin URI:        https://gitlab.com/emugabi/jenga-checkout.git
 * Description:       Jenja Checkout plugin brings the Jenga Payment Gateway functions to wordpress. It includes an admin panel where developer API credentials are safely stored, automatically refreshed tokens and includes parameterised shortcodes for a dynamic checkout flows.
 * Version:           1.0.7
 * Author:            Mugabi Elvis (A23 LABS)
 * Author URI:        https://github.com/emugabi
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       jenja-checkout
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.7' );

if(!defined('JENGA_URL'))
	define('JENGA_URL', plugin_dir_url( __FILE__ ));

if(!defined('JENGA_ENDPOINT'))
	define('JENGA_ENDPOINT', 'https://api-test.equitybankgroup.com');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/jenga-checkout-activator.php
 */
function activate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/jenga-checkout-activator.php';
	Jenga_Checkout_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/jenga_checkout-deactivator.php
 */
function deactivate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/jenga-checkout-deactivator.php';
	Jenga_Checkout_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_plugin_name' );
register_deactivation_hook( __FILE__, 'deactivate_plugin_name' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/jenga-checkout.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin_name() {

	$plugin = new Jenga_Checkout();
	$plugin->run();

}
run_plugin_name();
