<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://gitlab.com/emugabi/jenga-checkout
 * @since      1.0.0
 *
 * @package    Jenga_Checkout
 * @subpackage Jenga_Checkout/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Jenga_Checkout
 * @subpackage Jenga_Checkout/public
 * @author     Your Name <elvis@a23labs.com>
 */
class Jenga_Checkout_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private $jengaUtils;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->jengaUtils = new JenjaUtils();

	}

	/**
	 * Register the Jenja Api Callbackup Routes
	 *
	 * @since    1.0.6
	 */
	public function register_api_endpoints(){

		register_rest_route( 'jenja-checkout/v1', '/callback', array(
			'methods'  => WP_REST_Server::READABLE,
			'callback' => 'jenja_checkout_callback',
		) );
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Jenga_Checkout_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Jenga_Checkout_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/jenga-checkout.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Jenga_Checkout_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Jenga_Checkout_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/jenga-checkout.js', array( 'jquery' ), $this->version, false );

	}

		public function render_jenga_checkout_form($attrs){

			// Get amount of money. Default amount is USD 10
			extract(shortcode_atts(array(
				'amount' => 10, 
			 ), $attrs));
	
			 $pluginData = $this->getData();
	
			 $logoUrl = "https://phaneroo.org/wp-content/uploads/2018/02/Phaneroo_logo-2.png";
			 $callback_url = get_site_url(null, "jenja-checkout/v1/callback");
			 $website = "https://phaneroo.org";
			 $orderReference = bin2hex(random_bytes(5));
			 $merchantName = "Phaneroo Ministries International";
			 $merchantCode = $pluginData['merchant_code'];
			 $expiryDateTime = "2025-02-17T19:00:00";
			 $token = $this->jengaUtils->getPaymentToken(true);
	
			 if(is_array($token))
				if(!array_key_exists('error', $token))
					$token =  $token['payment-token'];
	
	
			?> 

			<form 
				id="eazzycheckout-payment-form"
				action=" https://api-test.equitybankgroup.com/v2/checkout/launch" method="POST">

				<div class="form-row align-items-center">

				<div class="col-sm-12 col-md-6">
					<label for="fullName">Full Name</label>
					<input type="text" class="form-control" name="custName" id="fullName" placeholder="First and Last Name" required>
  				</div>

				  <div class="col-sm-12 col-md-6">
					<label for="contactInformation">Email</label>
					<input type="email" class="form-control" id="contactInformation" placeholder="Email" required>
  				</div>

				</div>

				<div class="form-row align-items-center">

					
			 	</div>

				<div class="form-row align-items-center">

					<div class="col-sm-12 col-md-6">
						<label for="exampleFormControlSelect1">Giving Reason</label>
						<select class="form-control" id="exampleFormControlSelect1">
							<option>OFFERTORY</option>
							<option>TITHE</option>
							<option>FIRST FRUIT</option>
							<option>PARTNERSHIP</option>
							<option>PRISONS MINISTRY</option>
							<option>RADIO/TV MINISTRY</option>
							<option>FAMILY MINISTRY</option>
							<option>CONFERENCES</option>
							<option>OTHER</option>
						</select>
					</div>

					<div class="col-sm-12 col-md-6">
						<div class="input-group" style="margin-top: 50px;">
								<div class="input-group-prepend">
									<div class="input-group-text">KES</div>
								</div>
								<input type="number" class="form-control"  min="5" max="1000" name="amount" value="" placeholder="Enter Amount" required>
							</div>
						</div>
			 		</div>

					 <div class="col-sm-12 col-md-6">
					 <div class="form-group">
    					<label for="commentInput">Comment/Remarks</label>
    					<textarea class="form-control" id="commentInput" rows="3"></textarea>
  					</div>
			 		</div>
				<input type="hidden" id="token" name="token" value="<?php echo isset($token) ? $token : 'err_missing_token'; ?>"/>
				<!-- <input type="hidden" id="amount" name="amount" value="<?php // echo isset($amount) ? $amount : 0 ?>"/> -->
				<input type="hidden" id="orderReference" name="orderReference" value="<?php echo $orderReference ?>">
				<input type="hidden" id="merchantCode" name="merchantCode" value="<?php echo $merchantCode ?>"/>
				<input type="hidden" id="merchant" name="merchant" value="<?php echo $merchantName ?>">
				<input type="hidden" id="website" name="website" value="<?php echo $website ?>">
				<input type="hidden" id="outletCode" name="outletCode" value="2609028424">
				<input type="hidden" id="popupLogo" name="popupLogo" value="<?php echo $logoUrl ?>">
				<input type="hidden" id="ez1_callbackurl" name="ez1_callbackurl" value="<?php echo $callback_url ?>"/>
				<input type="hidden" id="expiry" name="expiry" value="<?php echo $expiryDateTime ?>">

				<div class="col-sm-12 col-md-6 mt-2">
					<button type="submit" style="background-color: #91C73D;" >GIVE</button>
				</div>

			</form>
	
			<?php
		}


		public function jenja_checkout_callback (WP_REST_Request $request){

			return "Peter";

		}

		public function getData(){
			return $this->jengaUtils->getPluginOptions();
		}

}
