<?php

require_once plugin_dir_path(( __FILE__ ) ) . 'includes/jenga-checkout-functions.php';
/**
 * Fired when the plugin is uninstalled.
 *
 *
 * @link       http://gitlab.com/emugabi/jenga-checkout
 * @since      1.0.0
 *
 * @package    Jenga_Checkout
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

// wipe plugin options

$jengaUtils = new JenjaUtils();

$jengaUtils->updatePluginOptions([]);
