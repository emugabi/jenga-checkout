# Jenga Checkout Plugin

Contributors: Mugabi Elvis  
Tags: jenga, checkout, mobile money  
Requires at least: 5.0  
Tested up to: x.x  
Requires PHP: 5.6  
Stable tag: 1.1  
License: GPLv2 or later  
License URI: http://www.gnu.org/licenses/gpl-2.0.html  

Jenja Checkout plugin brings the Jenga Payment Gateway functions to wordpress. 

## Description

Jenja Checkout plugin brings the Jenga Payment Gateway functions to wordpress. It includes an admin panel where developer API credentials are safely stored, automatically refreshed tokens and includes parameterised shortcodes for a dynamic checkout flows.

## Installation 
1. Upload `jenga-checkout.zip` to the `/wp-content/plugins/` directory.
2. Activate the plugin through the `Plugins` menu in WordPress.


## Frequently Asked Questions 
How do I get API keys?  =
Visit the official jenga payment gateway documentation here - https://developer.jengapgw.io/

How does the short code work =
Use the `[jenga-checkout]` shortcode to create a checkout button on any page or post.   

To change the payment attributes and button styling, simply pass the following parameters;

- amount [jenga-checkout `amount=123456`]
- merchantName [jenga-checkout `merchantName=abcdef`]
- checkoutBtnColor [jenga-checkout `checkoutBtnColor=#fffff`]
- checkoutBtnText [jenga-checkout `checkoutBtnText=Checkout`]
- paymentReference [jenga-checkout `paymentReference=abcdef`]